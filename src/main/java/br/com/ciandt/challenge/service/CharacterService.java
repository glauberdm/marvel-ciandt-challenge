package br.com.ciandt.challenge.service;

import br.com.ciandt.challenge.marvel.api.ChangeControlApi;
import br.com.ciandt.challenge.marvel.api.MarvelApi;
import br.com.ciandt.challenge.model.Character;
import br.com.ciandt.challenge.repository.CharacterRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Character Service.
 *
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
@Service
public class CharacterService {

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private ComicService comicService;

    public ResponseEntity<List<Character>> getAll() {

        Map<String, String> addtionalHeaders = new HashMap<>();
        addtionalHeaders.put("If-None-Match", characterRepository.getEtag());

        Response response = MarvelApi.getPath(addtionalHeaders, "characters");

        if (!response.getStatusInfo().equals(Response.Status.NOT_MODIFIED)) {
            List<Character> characters = processResponseDataListResults(response, characterRepository);
            characterRepository.saveAll(characters);
        }

        List<Character> characters = characterRepository.findAll();

        for (Character character : characters) {
            comicService.getComicsByCharacter(character);
        }

        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    public ResponseEntity<Character> getCharacterBy(Character character) {
        Map<String, String> addtionalHeaders = new HashMap<>();
        addtionalHeaders.put("If-None-Match", characterRepository.getEtag());

        Response response = MarvelApi.getPath(addtionalHeaders, "characters", character.getId().toString());

        if (!response.getStatusInfo().equals(Response.Status.NOT_MODIFIED)) {
            character = processResponseDataListResults(response, character).get(0);
            characterRepository.save(character);
        }

        character = characterRepository.findById(character.getId());

        return new ResponseEntity<>(character, HttpStatus.OK);
    }


    private List<Character> processResponseDataListResults(Response response, ChangeControlApi changeControlApi) {
        List<Character> characters = new ArrayList<>();

        if (response.getStatusInfo().equals(Response.Status.OK)) {
            String entity = response.readEntity(String.class);

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            JsonObject jsonObject = gson.fromJson(entity, JsonObject.class);

            changeControlApi.setEtag(jsonObject.get("etag").toString());

            JsonElement jsonElement = jsonObject.get("data").getAsJsonObject().get("results");

            Type typeList = new TypeToken<List<Character>>() {
            }.getType();

            characters = gson.fromJson(jsonElement, typeList);
        }

        return characters;
    }
}
