package br.com.ciandt.challenge.service;

import br.com.ciandt.challenge.marvel.api.ChangeControlApi;
import br.com.ciandt.challenge.marvel.api.MarvelApi;
import br.com.ciandt.challenge.model.Character;
import br.com.ciandt.challenge.model.Comic;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
@Service
public class ComicService {

    @Autowired
    private CharacterService characterService;

    public ResponseEntity<List<Comic>> getComicsByCharacter(Character character) {

        character = characterService.getCharacterBy(character).getBody();

        Map<String, String> addtionalHeaders = new HashMap<>();
        addtionalHeaders.put("If-None-Match", character.getEtag());
        Response response = MarvelApi.getPath(addtionalHeaders, "characters", character.getId().toString(), "comics");

        if (!response.getStatusInfo().equals(Response.Status.NOT_MODIFIED)) {
            List<Comic> comics = processResponseDataListResults(response, character);
            character.setComics(comics);
        }

        List<Comic> comics = character.getComics();

        return new ResponseEntity<>(comics, HttpStatus.OK);
    }

    private List<Comic> processResponseDataListResults(Response response, ChangeControlApi changeControlApi) {
        List<Comic> comics = new ArrayList<>();

        if (response.getStatusInfo().equals(Response.Status.OK)) {
            String entity = response.readEntity(String.class);

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            JsonObject jsonObject = gson.fromJson(entity, JsonObject.class);

            changeControlApi.setEtag(jsonObject.get("etag").toString());

            JsonElement jsonElement = jsonObject.get("data").getAsJsonObject().get("results");

            Type typeList = new TypeToken<List<Comic>>() {
            }.getType();

            comics = gson.fromJson(jsonElement, typeList);
        }

        return comics;
    }
}
