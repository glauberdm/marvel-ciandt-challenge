package br.com.ciandt.challenge.repository;

import br.com.ciandt.challenge.marvel.api.ChangeControlApi;
import br.com.ciandt.challenge.model.Character;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
@Component
public class CharacterRepository extends Repository<Character> implements ChangeControlApi {

    public Character findByName(String name) {
        return dataMap.entrySet().parallelStream().filter( p -> p.getValue().getName().equals(name)).findFirst().get().getValue();
    }
}
