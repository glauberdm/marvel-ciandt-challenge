package br.com.ciandt.challenge.repository;

import br.com.ciandt.challenge.marvel.api.ChangeControlApi;
import br.com.ciandt.challenge.model.Comic;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
@Component
public class ComicRepository extends Repository<Comic> implements ChangeControlApi {
}
