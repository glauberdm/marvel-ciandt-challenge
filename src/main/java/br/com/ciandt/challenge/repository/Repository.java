package br.com.ciandt.challenge.repository;

import br.com.ciandt.challenge.model.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public abstract class Repository<T extends Model> {

    protected Map<Long, T> dataMap = new HashMap<>();

    private String etag;

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public T findById(Long id) {
        return (T) dataMap.entrySet().parallelStream().filter(p -> p.getKey().equals(id)).findFirst().get().getValue();
    }

    public void save(T model) {
        dataMap.put(model.getId(), model);
    }

    public List<T> findAll() {
        return new ArrayList<T>(dataMap.values());
    }

    public void saveAll(List<T> models) {
        dataMap.putAll(models.parallelStream().collect(Collectors.toMap(i -> i.getId(), i -> i)));
    }
}
