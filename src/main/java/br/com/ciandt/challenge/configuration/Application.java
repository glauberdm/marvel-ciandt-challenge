package br.com.ciandt.challenge.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Marvel CI&T Application.
 *
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("br.com.ciandt.challenge")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
