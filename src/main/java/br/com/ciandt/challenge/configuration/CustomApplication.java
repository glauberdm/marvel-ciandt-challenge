package br.com.ciandt.challenge.configuration;

import br.com.ciandt.challenge.handler.GsonMessageBodyHandler;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public class CustomApplication extends ResourceConfig {

    public CustomApplication() {
        register(GsonMessageBodyHandler.class);
    }

}
