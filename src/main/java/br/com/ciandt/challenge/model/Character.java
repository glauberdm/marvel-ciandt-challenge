package br.com.ciandt.challenge.model;

import br.com.ciandt.challenge.marvel.api.ChangeControlApi;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Character entity type.
 *
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public class Character implements Model, ChangeControlApi {
    @Expose
    private Long id;

    @Expose
    private String name;

    @Expose
    private String description;

    @Expose
    private String resourceURI;

    private String etag;

    private List<Comic> comics;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public List<Comic> getComics() {
        return comics;
    }

    public void setComics(List<Comic> comics) {
        this.comics = comics;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }
}
