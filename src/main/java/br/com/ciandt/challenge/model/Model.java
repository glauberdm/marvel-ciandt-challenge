package br.com.ciandt.challenge.model;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public interface Model {
    Long getId();
}
