package br.com.ciandt.challenge.model;

import br.com.ciandt.challenge.marvel.api.ChangeControlApi;
import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

/**
 * Comic entity type.
 *
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public class Comic implements Model, ChangeControlApi {
    @Expose
    private Long id;

    @Expose
    private String title;

    @Expose
    private String variantDescription;

    @Expose
    private String description;

    @Expose
    private String isbn;

    @Expose
    private Long pageCount;

    @Expose
    private String resourceURI;

    private List<Character> characters;

    private String etag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVariantDescription() {
        return variantDescription;
    }

    public void setVariantDescription(String variantDescription) {
        this.variantDescription = variantDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }

    @Override
    public String getEtag() {
        return etag;
    }

    @Override
    public void setEtag(String etag) {
        this.etag = etag;
    }
}
