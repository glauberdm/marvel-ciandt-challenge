package br.com.ciandt.challenge.marvel.api;

import br.com.ciandt.challenge.md5.MD5Encoder;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * Marvel Api Client.
 *
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public class MarvelApi {
    private static final String uri = "http://gateway.marvel.com/v1/public";
    private static final String apiKey = "1384bb78bc59e98bd66e00506cc4d2eb";
    private static final String privateKey = "7c9775ab3da61ea113d16233ef84456686176c3c";

    public static String getUri() {
        return uri;
    }

    public static String getApiKey() {
        return apiKey;
    }

    public static String getHash(String ts) {
        MD5Encoder md5Encoder = new MD5Encoder();

        String hashBase = new StringBuilder()
                .append(ts)
                .append(privateKey)
                .append(apiKey)
                .toString();

        return md5Encoder.encoding(hashBase.getBytes());
    }

    public static Response getPath(Map<String, String> additionalHeaders, String... paths) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(MarvelApi.getUri());

        Response response = responseTargetWithAdditionalHeaders(webTarget, additionalHeaders, paths);

        return response;
    }

    public static Response getPath(String... paths) {
        return getPath(new HashMap<>(), paths);
    }

    private static Response responseTargetWithAdditionalHeaders(WebTarget webTarget, Map<String, String> additionalHeaders, String... paths) {
        final String timeStamp = LocalDate.now().toString();

        for (String path : paths) {
            webTarget = webTarget.path(path);
        }

        Invocation.Builder invocationBuilder = webTarget
                .queryParam("apikey", MarvelApi.getApiKey())
                .queryParam("ts", timeStamp)
                .queryParam("hash", MarvelApi.getHash(timeStamp))
                .request()
                .accept(MediaType.WILDCARD);

        for (Map.Entry<String, String> headerEntry : additionalHeaders.entrySet()) {
            invocationBuilder.header(headerEntry.getKey(), headerEntry.getValue());
        }

        Response response = invocationBuilder.get();

        return response;
    }
}
