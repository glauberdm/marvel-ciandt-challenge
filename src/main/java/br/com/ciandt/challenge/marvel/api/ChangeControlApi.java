package br.com.ciandt.challenge.marvel.api;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public interface ChangeControlApi {
    String getEtag();

    void setEtag(String etag);
}
