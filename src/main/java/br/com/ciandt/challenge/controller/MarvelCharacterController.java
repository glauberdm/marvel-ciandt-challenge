package br.com.ciandt.challenge.controller;

import br.com.ciandt.challenge.model.Character;
import br.com.ciandt.challenge.model.Comic;
import br.com.ciandt.challenge.service.CharacterService;
import br.com.ciandt.challenge.service.ComicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Marvel CI&T Character Controller.
 *
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
@RestController
@RequestMapping(value = "/marvel/characters")
public class MarvelCharacterController {

    @Autowired
    private CharacterService characterService;

    @Autowired
    ComicService comicService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Character>> getAll() {
        return characterService.getAll();
    }

    @RequestMapping(value = "/{characterId}", method = RequestMethod.GET)
    public ResponseEntity<Character> getCharacter(@PathVariable Long characterId) {
        Character character = new Character();
        character.setId(characterId);
        return characterService.getCharacterBy(character);
    }

    @RequestMapping(value = "/{characterId}/comics", method = RequestMethod.GET)
    public ResponseEntity<List<Comic>> getComics(@PathVariable Long characterId) {
        Character character = new Character();
        character.setId(characterId);
        return comicService.getComicsByCharacter(character);
    }
}
