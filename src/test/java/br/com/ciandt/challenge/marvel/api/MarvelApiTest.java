package br.com.ciandt.challenge.marvel.api;

import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Marvel Api Client Test
 *
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public class MarvelApiTest {

    @Test
    public void marvelApiAccess() {
        final String timeStamp = LocalDate.now().toString();
        Client client = ClientBuilder.newClient();

        Response response = client
                .target("http://gateway.marvel.com/v1/public/comics")
                .queryParam("apikey", MarvelApi.getApiKey())
                .queryParam("ts", timeStamp)
                .queryParam("hash", MarvelApi.getHash(timeStamp))
                .request()
                .accept(MediaType.WILDCARD)
                .get();

        assertThat(response.getStatusInfo(), is(Response.Status.OK));
    }

    @Test
    public void marvelApiGetComics() {
        Response response = MarvelApi.getPath("comics");

        assertThat(response.getStatusInfo(), is(Response.Status.OK));
    }

    @Test
    public void marvelApiGetCharacters() {
        Response response = MarvelApi.getPath("characters");

        assertThat(response.getStatusInfo(), is(Response.Status.OK));
    }

    @Test
    public void marvelApiGetCreators() {
        Response response = MarvelApi.getPath("creators");

        assertThat(response.getStatusInfo(), is(Response.Status.OK));
    }

    @Test
    public void marvelApiGetCreators1(){
        Response response = MarvelApi.getPath("creators","1");

        assertThat(response.getStatusInfo(), is(Response.Status.OK));
    }

    @Test
    public void marvelApiGetCreators1Comics(){
        Response response = MarvelApi.getPath("creators","1","comics");

        assertThat(response.getStatusInfo(), is(Response.Status.OK));
    }
}
