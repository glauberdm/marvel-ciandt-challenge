package br.com.ciandt.challenge.md5;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author <a href="mailto:glauberbcc@gmail.com">Glauber Monteiro</a>
 */
public class MD5EncoderTest {
    @Test
    public void encoding() throws Exception {
        final String MD5_TESTE = "99a29dc8105fd2fa39d8cdc04733938d";
        MD5Encoder md5Encoder = new MD5Encoder();


        String md5 = md5Encoder.encoding("TESTE" .getBytes());


        assertThat(md5, is(MD5_TESTE));
    }

}