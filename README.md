# Marvel CI&T Application #

### Requisites ###

* Java 8
* Maven 3.2.x or greater

### Run ###

Three ways for run.

* Spring Boot Run

#### Spring Boot Run ####

```
#!console
../marvel-ciandt-challenge$ mvn spring-boot:run
```

### Test ###

* Get all characters: http://localhost:8080/marvel/characters
* Get specific character: http://localhost:8080/marvel/characters/1011334
* Get comics of character: http://localhost:8080/marvel/characters/1011334/comics